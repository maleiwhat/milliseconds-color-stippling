# Milliseconds Color Stippling

A Open repository for my paper "Milliseconds Color Stippling", accepted by ACM MM 2021.

**Milliseconds Color Stippling**

Lei Ma, Jian Shi, Yanyun Chen.

Stippling is a popular and fascinating sketching art in stylized illustrations. Various digital stippling techniques have been proposed to reduce tedious manual work. In this paper, we present a novel method to create high-quality color stippling from an input image in milliseconds. The key idea is to obtain stipples with predetermined incremental 2D sample sequences, which algorithms generate with sequential incrementality and distributional uniformity features. Two typical sequences are employed in our work: one is constructed from incremental Voronoi sets, and the other is from Poisson disk distributions. A threshold-based algorithm is then applied to determine stipple appearance and guarantee result quality. We extend color stippling with multitone level and radius adjustment to achieve improved visual quality. Detailed comparisons of the two sequences are conducted to explore further the strengths and weaknesses of the proposed method.

**Results**
![Image text](https://gitlab.com/maleiwhat/milliseconds-color-stippling/-/raw/main/teaser.png)

![Image text](https://gitlab.com/maleiwhat/milliseconds-color-stippling/-/raw/main/Spectrum-of-channels.png)

![Image text](https://gitlab.com/maleiwhat/milliseconds-color-stippling/-/raw/main/ice-cream.jpg)

**Code Coming Soon**

We are finishing up our code and uploading it here soon.

**Maintainer**

Lei Ma, lei.ma@pku.edu.cn
Jian Shi, jian.shi@ia.ac.cn


